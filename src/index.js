const gemini = require('gemini-server');
const gql = require('gql-query-builder');
const { GitlabService } = require('./gitlab');
const { Config } = require('./config');
const fs = require('fs');
const Lazy = require('lazy.js');
const _ = require('lodash');

const gitlab = new GitlabService(process.env['GITLAB_TOKEN']);
const config = new Config();

const app = gemini({
    cert: fs.readFileSync(config.cert),
    key: fs.readFileSync(config.key),
});

app.on('/user/:username/project', async (req, res) => {
    function generateBody(username) {
        const stream = gitlab.streamProjectsByUser(username);
        return new Promise((resolve, reject) => {
            let body = `# GitLab projects - ${username}\n\n`;
            stream.on('data', (project) => {
                const topics = _.isArray(project.topics) ? project.topics : [];
                const lines = [
                    `# ${project.fullPath}`,
                    `${project.starCount} Stars - ${project.forksCount} Forks`,
                ];
                if (topics.length > 0) {
                    project.push(`[ ${Lazy(topics).join(', ')} ]`);
                }
                lines.push(`=> ${gitlab.projectPath(project.fullPath)} ${project.name}`);
                if (_.isString(project.description) && project.description.length > 0) {
                    lines.push(`> ${project.description}`);
                }
                body += Lazy(lines).join('\n');
                body += '\n\n';
            });
            stream.on('error', (e) => reject(e));
            ['end', 'close'].forEach((evt) => {
                stream.on(evt, () => {
                    console.error(`stream.on(${evt})`);
                    return resolve(body);
                });
            });
            stream.on('end', () => resolve(body));
        });
    }
    let body = await generateBody(req.params.username || '');
    body += '# about this capsule\n\n=> gemini://gemini.mcoffin.dev/projects/gemitlab/ About this capsule\n';
    res.data(body, 'text/gemini');
});

app.listen(() => {
    console.log(`listening on ${config.port}`);
}, config.port);

// Promise.all(['mcoffin', 'mrchapp'].map((u) => {
//     const stream = gitlab.streamProjectsByUser(u);
//     return new Promise((resolve, reject) => {
//         stream.on('data', (v) => console.log(JSON.stringify(v, null, 4)));
//         stream.on('error', (e) => reject(e));
//         ['close', 'done'].forEach((evt) => {
//             stream.on(evt, () => resolve());
//         });
//     });
// }));
