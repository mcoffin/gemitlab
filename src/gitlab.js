const axios = require('axios');
const gql = require('gql-query-builder');
const _ = require('lodash');
const { Readable } = require('stream');

class ProjectsByUserStream extends Readable {
    constructor(gitlab, username) {
        super({
            objectMode: true,
            readableObjectMode: true,
        });
        this.gitlab = gitlab;
        this.username = username;
        this.resultsBuffer = [];
        this.pageInfo = null;
    }

    flushBuffer() {
        let pushResult = true;
        while (pushResult && this.resultsBuffer.length > 0) {
            pushResult = this.push(this.resultsBuffer[0]);
            this.resultsBuffer = this.resultsBuffer.slice(1);
        }
        return pushResult;
    }

    get isDone() {
        return !_.get(this.pageInfo || {}, 'hasNextPage', true);
    }

    get cursor() {
        return _.get(this.pageInfo || {}, 'endCursor');
    }

    async _realRead(size) {
        if (!this.flushBuffer()) {
            return;
        }

        if (this.isDone) {
            this.push(null);
            return;
        }

        const { nodes, pageInfo } = await this.gitlab.getProjectsByUser(this.username, this.cursor);
        this.resultsBuffer = nodes;
        this.pageInfo = pageInfo;
        const fullyFlushed = this.flushBuffer();
        if (fullyFlushed && this.isDone) {
            this.push(null);
            return;
        }
    }

    _read(size) {
        this._realRead(size).catch((e) => this.emit('error', e));
    }
}

class GitlabService {
    constructor(accessToken, baseURL = "https://gitlab.com") {
        this.baseURL = baseURL;
        this.axios = axios.create({
            baseURL: baseURL,
            headers: {
                'Authorization': `Bearer ${accessToken}`,
            },
        });
    }

    projectPath(projectFullPath) {
        return `${this.baseURL}/${projectFullPath}`;
    }

    streamProjectsByUser(username) {
        return new ProjectsByUserStream(this, username);
    }

    async getProjectsByUser(username, after) {
        const membershipsFields = [
            {
                nodes: [
                    {
                        project: [
                            'name',
                            'fullPath',
                            'description',
                            'visibility',
                            'starCount',
                            'forksCount',
                            'topics',
                        ],
                    },
                ],
            },
            {
                pageInfo: [
                    'endCursor',
                    'hasNextPage',
                ],
            },
        ];
        let projectMemberships = {
            projectMemberships: membershipsFields,
        };
        if (!_.isUndefined(after)) {
            projectMemberships = {
                operation: 'projectMemberships',
                variables: {
                    after,
                },
                fields: membershipsFields,
            };
        }
        const query = gql.query({
            operation: 'user',
            variables: {
                username: username,
            },
            fields: [
                projectMemberships
            ],
        });
        console.error('query:', query);
        const result = await this.axios.post('/api/graphql', query)
            .then(_.property('data.data.user.projectMemberships'))
            .then((v) => {
                if (!v) {
                    return {
                        nodes: [],
                        pageInfo: {
                            endCursor: '',
                            hasNextPage: false,
                        },
                    };
                }
                if (_.isArray(v.nodes)) {
                    v.nodes = v.nodes
                        .map(_.property('project'))
                        .filter((project) => project.visibility === 'public');
                }
                return v;
            });
        console.error('result:', result);
        return result;
    }
}

module.exports = {
    GitlabService,
};
