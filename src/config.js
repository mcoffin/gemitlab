const _ = require('lodash');
const minimist = require('minimist');

class MinimistConfig {
    constructor(minimistOptions = {}, args = process.argv.slice(2)) {
        this.minimistOptions = minimistOptions;
        this.args = args;
        this.config = minimist(this.args, this.minimistOptions);
    }

    get(...args) {
        return _.get(this.config, ...args);
    }

    get trailing() {
        const ret = this.get('_', []);
        if (_.isArray(ret)) {
            return ret;
        }
        if (_.isUndefined(ret) || _.isNull(ret)) {
            return [];
        }
        return [ret];
    }
}

class Config extends MinimistConfig {
    constructor(args) {
        super({
            string: [
                'cert',
                'key',
            ],
            alias: {
                'port': ['p'],
            },
            default: {
                'port': 1965,
            },
        }, args);
    }

    get cert() {
        return this.get('cert');
    }

    get key() {
        return this.get('key');
    }

    get port() {
        return this.get('port', 1965);
    }
}

module.exports = {
    Config,
};
